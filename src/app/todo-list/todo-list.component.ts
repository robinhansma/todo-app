import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../todo.reducer';
import {Observable} from 'rxjs';
import {State} from '../todo.reducer';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos: Observable<State>;

  constructor(private store: Store<fromRoot.State>) {
    this.todos = store.select('todo');
  }

  ngOnInit() {
  }

}
