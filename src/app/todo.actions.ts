import { Action } from '@ngrx/store';

export enum TodoActionTypes {
  LoadTodos = '[Todo] Load Todos',
  AddTodo =  '[Todo] Add Todo',
  RemoveTodo =  '[Todo] Remove Todo',
}

export class LoadTodos implements Action {
  readonly type = TodoActionTypes.LoadTodos;
}

export class AddTodo implements Action {
  readonly type = TodoActionTypes.AddTodo;

  constructor(public payload: string) {}
}

export class RemoveTodo implements Action {
  readonly type = TodoActionTypes.RemoveTodo;

  constructor(public payload: string) {}
}

export type TodoActions = LoadTodos | AddTodo | RemoveTodo;
