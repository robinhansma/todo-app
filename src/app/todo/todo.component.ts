import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';

import * as fromRoot from '../todo.reducer';
import * as todoActions from '../todo.actions';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todo = new FormControl('');

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {

  }

  onSubmit() {
    this.store.dispatch(new todoActions.AddTodo(this.todo.value));
    this.todo.setValue('');
  }

  onKeyDown(event) {
    if(event.key === 'Enter') {
      this.onSubmit();
    }
  }
}
