import {TodoActions, TodoActionTypes} from './todo.actions';

export interface Todo {
  value: string;
  id: string;
}

export interface State {
  todos?: Todo[];
}

export const initialState: State = {
  todos: []
};

export function reducer(state = initialState, action: TodoActions): State {
  switch (action.type) {

    case TodoActionTypes.LoadTodos:
      return state;

    case TodoActionTypes.AddTodo:
      if (_validateTodo(action.payload)) {
        const todo = {
          value: action.payload,
          id: _generateId()
        };
        return {...state, todos: [...state.todos, todo]};
      } else {
        return {...state};
      }

    case TodoActionTypes.RemoveTodo:
      return {...state, todos: state.todos.filter((elem) => elem.id !== action.payload)};

    default:
      return state;
  }
}


function _generateId() {
  return '_' + Math.random().toString(36).substr(2, 9);
}

function _validateTodo(todo) {
  return todo.length > 0;
}
