import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../todo.reducer';
import * as todoActions from '../todo.actions';
import {Todo} from '../todo.reducer';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input()
  value: Todo;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
  }

  remove() {
    this.store.dispatch(new todoActions.RemoveTodo(this.value.id));
  }

}
